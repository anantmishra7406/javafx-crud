package application;

import javafx.fxml.FXML;

import javafx.scene.control.TextField;

import javafx.scene.control.TableView;

import javafx.scene.control.TableColumn;

public class MainSceneController {
	@FXML
	private TextField txtID;
	@FXML
	private TextField txtNAME;
	@FXML
	private TextField txtAGE;
	@FXML
	private TextField txtSKILLS;
	@FXML
	private TableView<?> txtTABLE;
	@FXML
	private TableColumn<?, ?> idCOLOUM;
	@FXML
	private TableColumn<?, ?> nameCOLOUM;
	@FXML
	private TableColumn<?, ?> ageCOLOUM;
	@FXML
	private TableColumn<?, ?> skillsCOLOUM;

}
