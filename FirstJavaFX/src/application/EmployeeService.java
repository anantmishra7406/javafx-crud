package application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.RestAPI.test.Repository.EmployeeRepository;
import com.RestAPI.test.bean.Employee;
import com.mysql.cj.x.protobuf.MysqlxDatatypes.Scalar.String;

@Service
public class EmployeeService {
	@Autowired
	public EmployeeRepository subjectRepo;

	public List<Employee> getAllSubjects()
	{
		List<Employee> subjects = new ArrayList<>();
		subjectRepo.findAll().forEach(subjects::add);
		return subjects;
	}

	public void addSubject(Employee subject) {
		subjectRepo.save(subject);
		
	}

	public void updateSubject(String id, Employee subject) {
		subjectRepo.save(subject);		
	}

	public void deleteSubject(String id) {
		((Object) subjectRepo).deleteById(id);
		
	}
}
